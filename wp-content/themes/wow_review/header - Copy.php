<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package veayo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/wow_review.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
      
 <link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:400,500,600,700|Montserrat:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,700,700i,900,900i" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

<header>
    <section class="header-top menu">
        <div class="container">
            <div class="row">
                <!-- Site logo -->
                <div class="col-md-4 col-sm-4 col-xs-12 logo">
                  <span  onclick="openNav()"> <img src="<?php echo get_template_directory_uri() ?>/images/menu1.png"></span>
                    <a href="http://v2rsolution.co.in/wow-airline-reviews/" title="The Airline Reviewer">
                      <!--   <img src="images/logo.png" alt="The Airline Reviewer" class="mainResposnive"> -->
                      <img alt="The Airline Reviewer" class="mainResposnive" src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>">
                    </a>
                </div>
                <span id="mySidenav" class="sidenav">
                  <a href="javascript:void(0)" class="closebtn" ><span>Hello Johen !</span><p onclick="closeNav()">&times;</p></a>
                <div class="col-md-5 wow_menu text-center padding_remove">
                    <nav class="">
                      <!-- Note : This Class use for hide some menu in desktop (class="desktop_hide") -->
                        <ul class="">
                           <li class="desktop_hide"><a href="" title="Home"><i class="fas fa-home"></i>Home</a></li>
                            <li><a href="<?php echo do_shortcode('[contact type="write_a_review"]') ?>" title="Write A Review"><i class="fas fa-edit"></i>Write A Review</a></li>
                            <li><a href="<?php echo do_shortcode('[contact type="search_review"]') ?>" title="Search Reviews"><i class="fas fa-search"></i>Search Reviews</a></li>
                            <li><a href="<?php echo do_shortcode('[contact type="recent_review"]') ?>" title="Recent Reviews"><i class="far fa-clock"></i>Recent Reviews</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-3 text-right padding_remove">
                    <ul class="login_process">
                        <a href="<?php echo do_shortcode('[contact type="login_register"]') ?>"><li title="login"><i class="fas fa-sign-in-alt"></i>Log In</li></a>
                        <a href="<?php echo do_shortcode('[contact type="login_register"]') ?>"><li title="Sign Up"><i class="fas fa-sign-out-alt"></i>Sign Up</li></a>
                    </ul>
                </div>
                </span>
            </div>
        </div>
    </section>
</header>
<section id="common_section">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
            <h1><span> Blog</span></h1>
            <p></p>
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('
                <p id="breadcrumbs">','</p>
                ');
                }
            ?>
         </div>
      </div>
   </div>
</section>

	<div id="content" class="site-content clearfix">
		<div class="content-wrap">
 
  