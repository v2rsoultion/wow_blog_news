<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package veayo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="keywords" content="best airline in the world, flight experience, top airlines, airline reviews, best international airlines, airline ranking, airline rating" />
  <meta name="description" content="Before getting any travelling experience read our blogs where we shared a lot information about travelling destination." />
  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/wow_review.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
      
 <link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:400,500,600,700|Montserrat:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,700,700i,900,900i" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

<header>
 <section class="header-top menu" data-spy="affix" data-offset-top="400">
        <div class="container">
            <div class="row">
                <!-- Site logo -->
                <div class="col-md-1 col-sm-12 col-xs-12 logo">
                  <span  onclick="openNav()"> <img src="<?php echo get_template_directory_uri() ?>/images/menu1.png"></span>
                    <a href="http://theairlinereviewer.com/" title="The Airline Reviewer">
                       <img src="<?php echo get_template_directory_uri() ?>/images/logolast.svg" alt="The Airline Reviewer" class="">
                      
                    </a>
                </div>
                
                <span id="mySidenav" class="sidenav">
                 <a href="javascript:void(0)" class="closebtn"><span><img src="<?php echo get_template_directory_uri() ?>/images/logolast.svg" alt="The Airline Reviewer" class="ressLogo"> </span><p onclick="closeNav()">&times;</p></a>
                <div class="col-md-8 col-sm-8 col-xs-12 wow_menu text-center padding_remove">
                    <nav class="">
                      <!-- Note : This Class use for hide some menu in desktop (class="desktop_hide") -->
                        <ul class="">
                           <li class="desktop_hide"><a href="" title="Home"><i class="fas fa-home"></i>Home</a></li>
                            <li><a href="<?php echo do_shortcode('[contact type="write_a_review"]') ?>" title="Write A Review"><i class="fas fa-edit"></i>Write A Review</a></li>
                            <li><a href="<?php echo do_shortcode('[contact type="search_review"]') ?>" title="Search Reviews"><i class="fas fa-search"></i>Search Reviews</a></li>
                            <li><a href="<?php echo do_shortcode('[contact type="recent_review"]') ?>" title="Recent Reviews"><i class="far fa-clock"></i>Recent Reviews</a></li>
                            <li><a href="http://blog.theairlinereviewer.com" title="The Travel Reviewer Blog"><i class="fab fa-blogger"></i>The Travel Reviewer Blog</a></li>
                            <li class="desktop_hide"><a href="http://theairlinereviewer.com/about-the-team" title="About The Team"><i class="fas fa-info-circle"></i>About The Team</a></li>
                        </ul>
                    </nav>
                </div>
               <div class="col-md-3 col-sm-4 col-xs-12 text-right padding_remove responsiveRight">
                  <ul class="login_process">
                  <a href="<?php echo do_shortcode('[contact type="login_register"]') ?>"><li title="Sign In"><i class="fas fa-sign-in-alt"></i>Sign In</li></a>  <span class="hidden-xs">I</span>
                        <a href="<?php echo do_shortcode('[contact type="login_register"]') ?>"><li title="Sign Up"><i class="fas fa-sign-out-alt"></i>Sign Up</li></a>
                      <li class="GoogleC"> <a href="https://play.google.com/store/apps/details?id=com.wowreviews" title="Play Store" target="_blank" > <span class="fab fa-android"></span>  </a></li>
                      <li class="Itunes"> <a href="https://itunes.apple.com/us/app/wow-airline-reviews/id1431630611?ls=1&mt=8" title="App Store" target="_blank" > <span class="fab fa-apple"></span>  </a></li>
                  </ul>
                </div>
                </span>
            </div>
        </div>
    </section>
</header>



<section id="common_section">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12 padding_remove">
            <h1><span> The Travel Reviewer Blog</span></h1>
            <p class="blog_tagline">Get inspired</p>
           
           <div>
          <?php 


             $blog_Url ="http://blog.theairlinereviewer.com";
             $mainSite_Url ="http://theairlinereviewer.com/";
             $blog_title1 ="The Travel Reviewer Blog";
             

                if ( function_exists('yoast_breadcrumb') ) { ?><a class="bredcmm" href="<?php echo $mainSite_Url ; ?>" title="Home "> Home / </a>
                                                        <a class="bredcmm1" href="<?php echo $blog_Url ; ?>" title="<?php echo  $blog_title1 ?>"> <?php echo  $blog_title1 ?> / </a><?php
                yoast_breadcrumb('
                <p id="breadcrumbs">','</p>
                ');
                }
            ?>
            </div>
         </div>
      </div>
   </div>
</section>

	<div id="content" class="site-content clearfix">
		<div class="content-wrap">
 
  