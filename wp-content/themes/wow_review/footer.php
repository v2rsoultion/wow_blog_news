<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package veayo
 */

?>
		</div>
	</div>
</div>
<footer>
    <section id="footer_main">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 padding_remove wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <a href="http://www.v2rsolution.co.in/wow_airline" title="WOW Reviews">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/footer.svg" class="footer_logo">
                        </a>
                        <p class="">
                       <?php echo do_shortcode('[contact type="about"]') ?>
                         </p>
                    
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 hidden-xs common_footer wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                    <h4>Quick Links</h4>
                    <ul>
                        <li><a href="http://www.v2rsolution.co.in/wow_airline/the-why-about-wow" title="About Wow Reviews">The Why About WOW</a></li>
                        <li><a href="http://www.v2rsolution.co.in/wow_airline/about-the-team" title="About The Team">About The Team</a></li>
                        <li><a href="http://www.v2rsolution.co.in/wow_airline/contact-us" title="Contact Us ">Contact Us</a></li>
                        
                        <li><a href="http://www.v2rsolution.co.in/wow_airline/privacy-policy" title="Privacy policy">Privacy policy</a></li>
                        <li><a href="http://www.v2rsolution.co.in/wow_airline/terms-conditions" title="Terms and conditions">Terms and conditions</a></li>
                    </ul>
                </div>
               
                <div class="col-md-4 col-sm-6 col-xs-12 common_footer remo_padding1 wow fadeInRight " data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 ">
                    <h4>Social Pages</h4>
<p> <?php echo do_shortcode('[contact type="footertext"]') ?></p><br>
                   <!--  <form class="form-horizontal" action="" id="newsletter">
                        <p>Lorem ipsum dolor sit amet, coseceur.</p>
                        <div class="form-group">
                            <div class="col-sm-8 col-xs-7 remo_padding1 ">
                                <input type="email " class="form-control " id="subscrition " placeholder="Enter your email " name="subscrition">

                            </div>
                            <div class="col-sm-4 col-xs-5 remo_padding1 ">
                                <button type="submit " class="btn btn-warning " title="Submit" alt="Submit">Submit</button>
                            </div>
                        </div>
                    </form> -->
                    <!-- Social Icons -->
                    <a href="<?php echo do_shortcode('[contact type="facebook"]') ?>"  title="facebook" class="facebookf " target="_blank"><i class="fa fa-facebook "></i></a>
                    <a href="<?php echo do_shortcode('[contact type="twitter"]') ?>" title="twitter" class="twitter_hover" target="_blank"><i class="fab fa-twitter "></i></a>
                    <!-- <a href="<?php echo do_shortcode('[contact type="google"]') ?>" class="google_hover" title="google " target="_blank"><i class="fab fa-google-plus-g "></i></a> -->
                    <a href="<?php echo do_shortcode('[contact type="instagram"]') ?>"  class="instagram_hover"title="instagram " target="_blank"><i class="fab fa-instagram "></i></a>
                    
                    <a href="http://www.linkedin.com/in/wow-airline-review-49a310158" title="Linkedin" class="instagram_hover" target="_blank"><i class="fab fa-linkedin-in font_icon"></i></a>
                    
                    <div class="col-sm-12 col-xs-12 footer_partners"> <p>Partners :- </p>
                <a href="http://www.mesmo.me/" title="MESMO" class="" target="_blank">
                    <img src="http://www.v2rsolution.co.in/wow_airline/public/images/mesmo.png" class="mesmo_image" title="MESMO" >
                    
                    </a>
            </div>
            
            <div style="
    color: #9ea2ad;
    font-weight: 600;
    font-size: 10px;
    position: absolute;
    margin-left: 135px;
    margin-top: 85px;
    /* padding-top: 59px; */
">Be local in a Global City</div>
                </div>
            </div>
        </div>
    </section>
    <section id="copyright">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.5s " data-wow-duration="0.5s " data-wow-offset="10 ">
                    <p>Copyright &copy; <?php echo date("Y") ?> The Airline Reviewer All Right Reserved.</p>
                </div>
            </div>
        </div>
    </section>
</footer>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script type="text/javascript">
   function openNav() {
  $("#mySidenav").css({"left": "0"});
}
function closeNav() {
  $("#mySidenav").css({"left": "-100%"});
}

</script>
<?php wp_footer(); ?>
</body>
</html>
